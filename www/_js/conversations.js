// Define possible scenes
var scene = [ 
  'Greg and Manny', 
  'Greg and Rowley at the Country Club', 
  'Reading is Fun Group', 
  'Shuffleboard' 
];

// Define possible conversations
// - The first in the list will always be shown to first-time visitors
// - 'Scenes' are an array of possible choices to be chosen at random from the 'scene' array, defined above
var conversation = [
  {
    'Scenes':     [0],
    'Speaker':   'Greg',
    'Response0': 'I\'m Greg. I bet you already know that since I\'m on my way to becoming famous. But that\'s enough about me. Who are you?',
    'Response1': 'I\'ve read so much today, my eyes feel like they might fall out of my head. How many minutes did you read today?',
    'Response2': 'You just scored a new reward. You should come back again and see if you can beat my reading minutes. I don\'t mean to brag or anything, but I\'m pretty awesome at this reading thing. See you around.'
  }, {
    'Scenes':     [1],
    'Speaker':   'Greg',
    'Response0': 'Hey! I\'m Greg. This is awesome readers territory. Welcome. Who am I chatting with?',
    'Response1': 'I do a lot of reading, so I like suggestions. Speaking of reading a lot, how many minutes did you read today?',
    'Response2': 'You earned a new reward for sharing your minutes. More downloadable activities will be available soon. Come back and check them out when you share your minutes. See you later.'
  }, {
    'Scenes':     [0,1,2,3],
    'Speaker':   'Greg',
    'Response0': 'Greg is my name and reading is my game. What\'s your name?',
    'Response1': 'I read for 75 minutes. How many minutes did you read today?',
    'Response2': 'You read, you shared and now you win. Now go enjoy your new activity download. Bye.'
  }, {
    'Scenes':     [2],
    'Speaker':   'Rowley',
    'Response0': 'Hello! It\'s Rowley! Welcome to our reading group. It\'s called the Reading Is Fun Group. What\'s your name?',
    'Response1': 'I started the day listening to Joshie. Then I read my favorite book for 80 minutes. I love spending the day with books. How many minutes did you read today?',
    'Response2': 'Now you have earned a new reward. Reading and winning make me so happy. It also makes me happy that you stopped by. Visit again to chat and win more awards!'
  }, {
    'Scenes':     [0,1,2,3],
    'Speaker':   'Rowley',
    'Response0': 'Hi! My friends call me Rowley. What is your name?',
    'Response1': 'I could read for a million minutes! Joshie says everyone should try to read something every day. How many minutes did you read today?',
    'Response2': 'Now you get a cool new reward. Hope to see you soon.'
  }, {
    'Scenes':     [0,1,2,3],
    'Speaker':   'Rowley',
    'Response0': 'Yay! You\'re here! I\'m Rowley. Reading is one of my favorite hobbies. Joshie says reading is a great hobby. Who are you?',
    'Response1': 'I can\'t believe how much reading I did today. I beat my own reading minutes record! How many minutes did you read today?',
    'Response2': 'Now I get to tell you the good news: You win a reward! You should keep visiting to earn all the different activity downloads. Don\'t forget to visit me again!'
  }, {
    'Scenes':     [3],
    'Speaker':   'Rodrick',
    'Response0': 'My name is Rodrick. Wise big brother. Drumming genius. Reading all-star. Who am I chatting with?',
    'Response1': 'Let\'s talk about reading minutes. Like I said, my record is tops. How many minutes did you read today?',
    'Response2': 'Now you get to reap the rewards. Enjoy your new activity. See you soon.'
  }, {
    'Scenes':     [0,1,2,3],
    'Speaker':   'Rodrick',
    'Response0': 'What\'s up? I\'m Rodrick, Greg\'s way cooler and smarter older brother. You probably know my band, Löded Diper – we\'re amazing. Who are you?',
    'Response1': 'Hey! I bet you can\'t beat me in reading minutes. How many minutes did you read today?',
    'Response2': 'You just unlocked a new reward. There are plenty of new activity downloads, so you should keep visiting and sharing your minutes.'
  }, {
    'Scenes':     [0,1,2,3],
    'Speaker':   'Rodrick',
    'Response0': 'Hey! Rodrick here. We\'re talking books and like my band, it\'s going to rock. What\'s your name?',
    'Response1': 'I did a lot of reading today. Think you can beat my reading time? How many minutes did you read today?',
    'Response2': 'You\'ve earned a new activity download. Even though it\'s not a Löded Diper album, it\'s still a pretty cool reward. Stop in again soon to win another reward.'
  }, {
    'Scenes':     [2],
    'Speaker':   'Mrs. Heffley',
    'Response0': 'Welcome to the Reading Is Fun book club! I\'m Greg\'s mom, Mrs. Heffley. What is your name?',
    'Response1': 'Like the name says, we think reading is fun! Alone or with friends, in the backyard or the library – we read all the time. How many minutes did you read today?',
    'Response2': 'Reading is already a great reward but you\'ve just earned a new activity download too. We have lots of different activities for those that share their reading minutes. Come back again to collect all of the rewards!'
  }, {
    'Scenes':     [3],
    'Speaker':   'Mrs. Heffley',
    'Response0': 'Hello. I\'m Mrs. Heffley, better known as Greg\'s mom. What\'s your name?',
    'Response1': 'How many minutes did you read today?',
    'Response2': 'Now you can download a new activity. Hope to chat again soon.'
  }, {
    'Scenes':     [0,1,2,3],
    'Speaker':   'Mrs. Heffley',
    'Response0': 'I\'m Mrs. Heffley. Glad you could join us today. Who am I chatting with?',
    'Response1': 'Reading is a great hobby. I think reading makes people smart and interesting. I read every day. How many minutes did you read today?',
    'Response2': 'Now you\'ve earned a new activity. Thanks for visiting and sharing your minutes. See you soon.'
  }, {
    'Scenes':     [0,1,2,3],
    'Speaker':   'Fregley',
    'Response0': 'Hello friend! My name is Fregley. Wanna help me name my secret freckle? Tell me your name and the fun can begin!',
    'Response1': 'Geez, I read a lot today. I could spend hours with my books! How many minutes did you read today?',
    'Response2': 'You win an award, my friend. You should come back again. The more minutes you read and share, the more you win! See you soon.'
  }, {
    'Scenes':     [0,1,2,3],
    'Speaker':   'Fregley',
    'Response0': 'Hi. It\'s Fregley. No worries. This is a booger-free zone! What\'s your name?',
    'Response1': 'How many minutes did you read today?',
    'Response2': 'Now you\'ve won a new activity. Enjoy your reward and don\'t forget to visit me again. Bye!'
  }, {
    'Scenes':     [0,1,2,3],
    'Speaker':   'Fregley',
    'Response0': 'Hello! Welcome to Fregley\'s House of Books. If I owned a bookstore that is what I would call it! Who are you?',
    'Response1': 'I\'ve already read 60 minutes today. How many minutes did you read?',
    'Response2': 'I bet you\'ll read a ton of minutes this summer. Now you get a new activity. There are some pretty cool rewards, so you should visit again to win more. Happy reading!'
  }
];