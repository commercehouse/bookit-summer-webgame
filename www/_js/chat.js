$(function() {

  // Function to get next conversation response
  var selectedConversation;
  var responseCount = 0;

  var getNextResponse = function(giveReward) {
    var $fromBubble = $('.chat-frame').find('.message.from:empty:first');
    var $toBubble   = $fromBubble.next();

    // Make user's last answer uneditable
    $fromBubble.prev().find('.answer').prop('readonly', true);

    // Show next message's typing indicator and scroll it into view
    $fromBubble.show().text(selectedConversation.Speaker + ' is typing...');
    $('.message-pane').each(function() {
      $(this).animate({ scrollTop: $(this)[0].scrollHeight - $(this).outerHeight() });
    });

    // Add next response and input after delay to simulate texting
    setTimeout(function() {
      $fromBubble.text(selectedConversation['Response' + responseCount]);
      $toBubble.find('.answer').val('');
      $toBubble.show();
      responseCount++;
      $('.message-pane').each(function() {
        $(this).animate({ 
          scrollTop: $(this)[0].scrollHeight - $(this).outerHeight() 
        }, function() {
          if (giveReward) {
            // Change Send button to Claim Reward button
            $('.send')
              .text('Claim Reward')
              .css({
                'color':            'hsl(0, 0%, 100%)',
                'background-color': 'hsl(39, 100%, 50%)'
              })
              .unbind('click')
              .on('click', function() {
                $('#content-upper .wrapper, #content-lower .wrapper').fadeOut().promise().done(function() {
                  $('.download .get-prize').click(function() { window.open(rewardURL); return false; });
                  $('.download').fadeIn();
                });
              })
          } else {
            $('.chat-frame').find('.answer:visible:last').focus(); 
          }
        });
      });
    }, 2500);
  }


  // Function to emulate user sending message to chat partner
  var messagesSent = 0;
  var censorList   = ['ahole','anus','ash0le','ash0les','asholes','ass','assmonkey','assface','assh0le','assh0lez','asshole','assholes','assholz','asswipe','azzhole','azzwipe','bassterds','bastard','bastards','bastardz','basterd','basterds','basterdz','biatch','bitch','bitches','blowjob','boff','boffing','butthole','buttwipe','c0ck','c0cks','c0k','carpetmuncher','cawk','cawks','clit','cnts','cntz','cock','cockhead','cockhead','cocks','cocksucker','crap','cum','cunt','cunts','cuntz','dick','dild0','dild0s','dildo','dildos','dilld0','dilld0s','dominatricks','dominatrics','dominatrix','dyke','enema','fag','fag1t','faget','fagg1t','faggit','faggot','fagit','fags','fagz','faig','faigs','fart','flipping','fuck','fucker','fuckin','fucking','fucks','fudgepacker','fuk','fukah','fuken','fuker','fukin','fukk','fukkah','fukken','fukker','fukkin','g00k','gay','gaybor','gayboy','gaygirl','gays','gayz','goddamned','h00r','h0ar','h0re','hells','hoar','hoor','hoore','jackoff','jap','japs','jerkoff','jisim','jiss','jizm','jizz','knob','knobs','knobz','kunt','kunts','kuntz','lesbian','lezzian','lipshits','lipshitz','masochist','masokist','massterbait','masstrbait','masstrbate','masterbaiter','masterbate','masterbates','mutha','fuker','motha','fucker','fuker','fukka','fukkah','fucka','fuchah','fukker','fukah','mothafucker','mothafuker','mothafukkah','mothafukker','motherfucker','motherfukah','motherfuker','motherfukkah','motherfukker','motherfucker','muthafucker','muthafukah','muthafuker','muthafukkah','muthafukker','mutha','n1gr','nastt','nasty','nigger','nigur','niiger','niigr','orafis','orgasim','orgasm','orgasum','oriface','orifice','orifiss','packi','packie','packy','paki','pakie','paky','pecker','peeenus','peeenusss','peenus','peinus','pen1s','penas','penis','penisbreath','penus','penuus','phuc','phuck','phuk','phuker','phukker','polac','polack','polak','poonani','pr1c','pr1ck','pr1k','pusse','pussee','pussy','puuke','puuker','queer','queers','queerz','qweers','qweerz','qweir','recktum','rectum','retard','sadist','scank','schlong','screwing','semen','sex','sexx','sexxx','sx','sht','sh1t','sh1ter','sh1ts','sh1tter','sh1tz','shit','shits','shitter','shitty','shity','shitz','shyt','shyte','shytty','shyt','skanck','skank','skankee','skankey','skanks','skanky','slut','sluts','slutty','slutz','sonofabitch','tit','turd','va1jina','vag1na','vagiina','vagina','vaj1na','vajina','vullva','vulva','w0p','wh00r','wh0re','whore','xrated','xxx','bch','bitch','blowjob','clit','arschloch','fuck','shit','ass','asshole','btch','b17ch','b1tch','bastard','bich','boiolas','buceta','c0ck','cawk','chink','cipa','clits','cock','cum','cunt','dildo','dirsa','ejakulate','fatass','fcuk','fuk','fux0r','hoer','hore','jism','kawk','l3itch','l3i+ch','lesbian','masturbate','masterbat','masterbat3','motherfucker','s.o.b.','mofo','nazi','nigga','nigger','nutsack','phuck','pimpis','pusse','pussy','scrotum','shemale','shi+','shitt','slut','smut','teets','tits','boobs','b00bs','teez','testical','testicle','titt','w00se','jackoff','wank','whoar','whore','damn','dyke','fuck','shit','@$$','amcik','andskota','arse','assrammer','ayir','bi7ch','bitch','bollock','breasts','buttpirate','cabron','cazzo','chraa','chuj','cock','cunt','d4mn','daygo','dego','dick','dike','dupa','dziwka','ejackulate','ekrem','ekto','enculer','faen','fag','fanculo','fanny','feces','feg','felcher','ficken','fitt','flikker','foreskin','fotze','fu','fuk','futkretzn','gay','gook','guiena','h0r','h4x0r','hell','helvete','hoer','honkey','huevon','hui','injun','jizz','kanker','kike','klootzak','kraut','knulle','kuk','kuksuger','kurac','kurwa','kusi','kyrpa','lesbo','mamhoon','masturbat','merd','mibun','monkleigh','mouliewop','muie','mulkku','muschi','nazis','nepesaurio','nigger','orospu','paska','perse','picka','pierdol','pillu','pimmel','piss','pizda','poontsee','poop','porn','p0rn','pr0n','preteen','pula','pule','puta','puto','qahbeh','queef','rautenberg','schaffer','scheiss','schlampe','schmuck','screw','sharmuta','sharmute','shipal','shiz','skrib','gadzooks','tarnation','horsefeathers','succotash','daggummit','codswallop','falderal','humbug','poppycock'];

  var sendMessage = function() {
    var visitorInput = $('.chat-frame').find('.answer:visible:last').val().trim();

    var displayError = function(message) {
      $('.error').text(message);
      $('.error').slideDown({duration:'fast', queue:false}).delay(3500).fadeOut('slow');
    }

    // Return if there is no response
    if (visitorInput.length == 0) {
      displayError('Oops! You need to enter a response.');
      return;
    }

    // Cue appropriate response
    if (messagesSent == 0) {
      // Return if response is non-alphabetical
      if (!visitorInput.match(/^[a-zA-Z' -]+$/)) {
        displayError('Oops! Letters only.');
        return;
      }
      // Return if response is in the censor list
      for (i = 0; i < censorList.length; i++) {
        if (visitorInput.match(new RegExp('\\b' + censorList[i] + '\\b'))) {
          displayError('Nope! This is curse-free territory.');
          return;
        }
      }
      // Get next response and increment
      getNextResponse();
      messagesSent++;
    } else if (messagesSent == 1) {
      // Return if response is not a number
      if (isNaN(visitorInput)) {
        displayError('Oops! Numbers only.');
        return;
      }
      // Return if response is 0 or over 5000 (yeah right, kid)
      if (visitorInput == 0) {
        displayError('You can do better than that!');
        return;
      } else if (visitorInput >= 5000) {
        displayError('Hey now! Let\'s not get carried away.');
        return;
      }
      // Get next response (with 'give reward' flag set to true) and increment
      getNextResponse(true);
      messagesSent++;
      // Update list of available conversations and save to cookie
      availableConversations.splice(conversationIndex, 1);
      Cookies.set('availableConversations', JSON.stringify(availableConversations), { expires: 90 });
      // Send user's minutes read entry to database
      userData.MinuteCount = visitorInput;
      sendReadingEntry();
    } else {
      return;
    }
  }


  // Function to send user's entered minutes to database
  var userData = {};
  var rewardURL;

  var sendReadingEntry = function() {
    $.ajax({
      url:         'http://bookit-2015-summer.cohodigital.com/readentry',
      dataType:    'jsonp',
      data:        userData,
      complete: function(data) {
        rewardURL = 'http://49e423c5fcf701f1210b-630f9ad8de7467e78cfcae816900cf96.r18.cf1.rackcdn.com/downloads/0_crossword_puzzle.pdf';
      }
    });
  }


  // Function to reset array of available conversations
  var availableConversations = [];

  var resetConversations = function() {
    for(i = 0; i < conversation.length; i++) {
      availableConversations.push(i);
    }
    Cookies.set('availableConversations', JSON.stringify(availableConversations), { expires: 90 });
  }


  // Duplicate chat frame HTML to simplify code maintenance
  $('#content-upper .chat-frame').children().clone().appendTo('#content-lower .chat-frame');


  // Dismiss initial dialog and get first chat response
  $('#content-upper .dialog-box').one('click', '.start-chat', function() {
    $('#content-upper .dialog-box').fadeOut().promise().done(function() {
      $('#content-upper .wrapper, #content-lower .wrapper').fadeIn().promise().done(function() {
        getNextResponse();
      });
    });
  });


  // Get read minutes and set progress bars accordingly
  var readMinutesGoal = 2000000;

  $.ajax({
    url:         'http://bookit-2015-summer.cohodigital.com/readmetric',
    dataType:    'jsonp',
    success: function(data) {
      var readMinutesActual     = data.ReadMinuteAmount;
      var readMinutesPercentage = (readMinutesActual / readMinutesGoal) * 100 + '%';

      $('.progress-bar .total .current').animate({'width': readMinutesPercentage}, 1000);
      $('.progress-bar .total').attr('title', readMinutesActual + ' minutes read so far!');
    }
  });


  // Fetch or reset available conversations based on user's previous visits
  var previousVisits = Cookies.get('previousVisits');

  if ( previousVisits == undefined ) {
    previousVisits = 0;
    resetConversations();
  } else {
    previousVisits++;
    availableConversations = JSON.parse(Cookies.get('availableConversations'));
    if (availableConversations.length == 0)
      resetConversations();
  }
  // Update visit count
  Cookies.set('previousVisits', previousVisits, { expires: 90 });


  // Select conversation 0 for new visitors, or a random available conversation for return visitors
  conversationIndex    = previousVisits == 0 ? 0 : Math.floor(Math.random() * availableConversations.length);
  selectedConversation = conversation[availableConversations[conversationIndex]];


  // Randomly select a scene from the selected conversations array of possible scenes
  var selectedScene = scene[selectedConversation.Scenes[Math.floor(Math.random() * selectedConversation.Scenes.length)]];


  // Initialize the scene and chat partner
  $('.scene').attr('src', '../_img/chat-scene-' + selectedScene.toLowerCase().replace(/[^a-z0-9\s]/gi, '').replace(/ /g,'_') + '.png');
  $('.nameplate .avatar').attr('src', '../_img/chat-avatar-' + selectedConversation.Speaker.toLowerCase().replace(/[^a-z0-9\s]/gi, '').replace(/ /g,'_') + '.jpg');
  $('.nameplate .speaker').text(selectedConversation.Speaker);


  // Mirror inputs between chat frames
  $('.chat-frame').find('.answer').on('keypress keyup blur', function(e) {
    var inputIndex = $(this).closest('.chat-frame').find('.answer').index(this);
    var inputField = $('.chat-frame').not($(this).parents()).find('.answer').get(inputIndex);
    $(inputField).val($(this).val());
  });


  // Send message when clicking Send button
  $('.send').on('click', sendMessage);


  // Send message when hitting Enter key
  $('.answer').on('keyup', function(e) {
    if (e.which == 13)
      sendMessage();
  });


  // Keep error box at top of message pane during scroll
  $('.message-pane').on('scroll', function() {
    $('.error').each(function() {
      $(this).css('top', $(this).closest('.message-pane').scrollTop());
    });
  });


  // Reload page when claiming a reward
  $('.get-prize').on('click', function() {
    location.reload();
  });

})