if (window.location.host == 'bookitsummer.com')
    window.location.replace('http://www.bookitsummer.com/');

$(function() {

    var targetDate     = new Date(2015, 05, 22, 12, 00, 00).getTime() + (new Date().getTimezoneOffset() * 60000) - (5 * 60 * 60000);

    var getCountdown = function() {
        var totalSeconds = (targetDate - Date.now())/1000;
        var days    = Math.floor(totalSeconds / (60 * 60 * 24));
        var hours   = Math.floor(totalSeconds / (60 * 60)) % 24;
        var minutes = Math.floor(totalSeconds / 60) % 60;
        var seconds = Math.floor(totalSeconds) % 60;

        if ( Date.now() >= targetDate ) {
            $('header .launched').show();
            $('header .clock').hide();
        } else {
            $('header .launched').hide();
            $('header .clock').show();

            $('header .clock .days p').html(days);
            $('header .clock .hours p').html(hours);
            $('header .clock .minutes p').html(minutes);
            $('header .clock .seconds p').html(seconds);

            setTimeout(getCountdown, 1000);
        }
    };

    getCountdown();
});

Date.withTimeZone = function(year, month, date, hour, minute, second, timezone) {
  return new Date(Date.UTC(year,month,date,hour,minute,second) - (timezone*60*60000));
}