#!/usr/bin/env bash

enable_mysql=0
enable_mongo=0

webroot=/vagrant/www

rootpass='Fn4lg9D24'

if [ $enable_mysql ]; then
    mysqldb=''
    mysqluser=''
    mysqlpass=''
    phpMyAdmin=4.2.11
fi