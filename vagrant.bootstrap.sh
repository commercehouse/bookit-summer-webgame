#!/usr/bin/env bash

source /vagrant/vagrant.settings.sh

apt-get update
apt-get install -y apache2 php5 php5-dev php5-cli php5-mcrypt

if [ $enable_mysql == 1 ]; then

    sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $rootpass"
    sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $rootpass"
    apt-get install -y mysql-server php5-mysql

    # setup mysql user
    echo "CREATE USER '$mysqluser'@'localhost' IDENTIFIED BY '$mysqlpass'" | mysql -uroot -p$rootpass
    echo "CREATE DATABASE $mysqldb" | mysql -uroot -p$rootpass
    echo "GRANT ALL ON $mysqldb.* TO '$mysqluser'@'localhost'" | mysql -uroot -p$rootpass
    echo "flush privileges" | mysql -uroot -p$rootpass

    # if mysqldump file exists, run it
    if [ -f /vagrant/mysqldump.sql ]; then
        mysql -u root -p$rootpass $mysqldb < /vagrant/mysqldump.sql
    fi

    # install phpmyadmin
    if [ ! -d /vagrant/pma ]; then
        cd /tmp
        wget -O ./pma.tar.gz http://downloads.sourceforge.net/project/phpmyadmin/phpMyAdmin/$phpMyAdmin/phpMyAdmin-$phpMyAdmin-english.tar.gz
        tar -xzvf ./pma.tar.gz
        mv ./phpMyAdmin-$phpMyAdmin-english /vagrant/pma
    fi

    # OPTIONAL: uncomment to dump mysql every 10 minutes
    # echo "*/10 * * * * mysqldump --user=$mysqluser -p$mysqlpass $mysqldb > /vagrant/mysqldump.sql" | crontab
fi

if [ $enable_mongo == 1 ]; then
    apt-get install -y mongodb php5-mongo
fi


# setup apache

rm -f /etc/apache2/sites-enabled/*
ln -s $webroot /var/www/vagrant

echo "
<VirtualHost *:80>
        #ServerName www.example.com

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/vagrant

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn, error, crit, alert, emerg.
        LogLevel info

        ErrorLog $dir_webroot/error.log
        CustomLog $dir_webroot/access.log combined
</VirtualHost>

<Directory /var/www/vagrant>
        AllowOverride All
        Allow from all
</Directory>
" > /etc/apache2/sites-available/vagrant.conf

ln -s ../sites-available/vagrant.conf /etc/apache2/sites-enabled/vagrant.conf

sed -i '/short_open_tag = Off/c short_open_tag = On' /etc/php5/apache2/php.ini
a2enmod rewrite

service apache2 restart
