#!/usr/bin/env bash

source /vagrant/vagrant.settings.sh

service apache2 start

# Import mysql dump on startup
if [ $enable_mysql == 1 ] && [ -f /vagrant/mysqldump.sql ]; then
    mysql -u root -p$rootpass $mysqldb < /vagrant/mysqldump.sql
fi
